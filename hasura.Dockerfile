FROM hasura/graphql-engine:v1.2.2.cli-migrations-v2
# Or any other image that is sutable

# All the commands that need to come here to load the metadata and migrations from the existing hasura cluster

# copy migrations to be autoloaded by hasura on container boot up
# COPY ./migrations /hasura-migrations

# copy metadata to be autoloaded by hasura on container boot up
COPY ./metadata /hasura-metadata
