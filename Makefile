up:
	docker-compose up -d

down:
	docker-compose down

build:
	docker build -f ./hasura.Dockerfile . -t myhasura:latest

# install hasura-cli - https://hasura.io/docs/1.0/graphql/manual/hasura-cli/install-hasura-cli.html
# Command to get metadata : Save it in to metadata directory
get-metadata:
	hasura metadata export --endpoint http://localhost:8081

# Command to get migrations: Save it as migrations directory
get-migrations:
	hasura migrate create init --from-server --endpoint http://localhost:8081

# Command to apply metadata : Save it in to metadata directory
set-metadata:
	hasura metadata apply --endpoint http://localhost:8081

# Command to apply migrations: Save it as migrations directory
set-migrations:
	hasura migrate apply --endpoint http://localhost:8081